echo "Starting system..."

arch=$(uname -i)

if [ "$arch" == 'x86_64' ];then
    echo "x86_64 CPU detected"
    docker-compose -f docker/docker-compose_x86.yml up -d

elif [ "$arch" == 'x86_32' ]; then
    echo "x86_32 CPU detected"
    docker-compose -f docker/docker-compose_x86.yml up -d

elif [ "$arch" == 'aarch64' ]; then
    echo "ARM CPU detected"
    docker-compose -f docker/docker-compose_arm64.yml up -d

fi

echo "System running"

