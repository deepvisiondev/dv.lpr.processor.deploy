echo "Stopping system..."

arch=$(uname -i)

if [ "$arch" == 'x86_64' ]; then
    echo "x86_64 CPU detected"
    docker-compose -f docker/docker-compose_x86.yml down

elif [ "$arch" == 'x86_32' ]; then
    echo "x86_32 CPU detected"
    docker-compose -f docker/docker-compose_x86.yml down

elif [ "$arch" == 'aarch64' ]; then
    echo "ARM CPU detected"
    docker-compose -f docker/docker-compose_arm64.yml down

fi

echo "System stopped"

